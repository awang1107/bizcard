var googleapi = {
    authorize: function(options) {
        var deferred = $.Deferred();

        //Build the OAuth consent page URL
        var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'code',
            scope: options.scope
        });

        //Open the OAuth consent page in the InAppBrowser
        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

        //The recommendation is to use the redirect_uri "urn:ietf:wg:oauth:2.0:oob"
        //which sets the authorization code in the browser's title. However, we can't
        //access the title of the InAppBrowser.
        //
        //Instead, we pass a bogus redirect_uri of "http://localhost", which means the
        //authorization code will get set in the url. We can access the url in the
        //loadstart and loadstop events. So if we bind the loadstart event, we can
        //find the authorization code and close the InAppBrowser after the user
        //has granted us access to their data.
        $(authWindow).on('loadstart', function(e) {
            var url = e.originalEvent.url;
            var code = /\?code=(.+)$/.exec(url);
            var error = /\?error=(.+)$/.exec(url);

            if (code || error) {
                //Always close the browser when match is found
                authWindow.close();
            }

            if (code) {
                //Exchange the authorization code for an access token
                $.post('https://accounts.google.com/o/oauth2/token', {
                    code: code[1],
                    client_id: options.client_id,
                    client_secret: options.client_secret,
                    redirect_uri: options.redirect_uri,
                    grant_type: 'authorization_code'
                }).done(function(data) {
                    deferred.resolve(data);
                }).fail(function(response) {
                    deferred.reject(response.responseJSON);
                });
            } else if (error) {
                //The user denied access to the app
                deferred.reject({
                    error: error[1]
                });
            }
        });

        return deferred.promise();
    }
};

var fbapi = {
    authorize: function(options) {
        var deferred = $.Deferred();
		//deferred.reject({ error: 'Not Implemented' });

        //Build the OAuth consent page URL
        var authUrl = 'https://www.facebook.com/dialog/oauth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'token',
            scope: options.scope
        });

        //Open the OAuth consent page in the InAppBrowser
        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

		//https://developers.facebook.com/docs/facebook-login/login-flow-for-web-no-jssdk/
		//http://aaronparecki.com/articles/2012/07/29/1/oauth2-simplified
        $(authWindow).on('loadstart', function(e) {
            var url = e.originalEvent.url;
            var token = /#access_token=(.+)$/.exec(url);
            var error = /#error=(.+)$/.exec(url);
			var access_token = token[1]

            if (access_token || error) {
                //Always close the browser when match is found
                authWindow.close();
            }

            if (access_token) {
                //Access token recieved
                deferred.resolve(access_token);
            } else if (error) {
                //The user denied access to the app
                deferred.reject({
                    error: error[1]
                });
            }
        });
        return deferred.promise();
    }
};

$(document).on('deviceready', function() {
    var $GloginButton = $('#google a');
    var $GloginStatus = $('#google p');
	var $fbloginButton = $('#fb a');
	var $fbloginStatus = $('#fb p');

    $GloginButton.on('click', function() {
        googleapi.authorize({
            client_id: '10845411425-3v6tsgias7gchqqo3qqhdpbde4ar69q0.apps.googleusercontent.com',
            client_secret: 'PDbrjOEXZK3t4ohf7u5HZ7H4',
            redirect_uri: 'http://localhost',
            scope: 'https://www.googleapis.com/auth/analytics.readonly'
        }).done(function(data) {
            $GloginStatus.html('Access Token: ' + data.access_token);
            $.mobile.navigate("#myprofileview")
        }).fail(function(data) {
            $GloginStatus.html(data.error);
        });
    });

	$fbloginButton.on('click', function() {
		fbapi.authorize({
            client_id: '1431790103709845',
            client_secret: 'ae0bcfade0a037e472a21e57fd94417b',
            redirect_uri: 'https://kk.bizcard.org',
            scope: 'email,user_photos,user_likes,user_about_me'
        }).done(function(data) {
             $.mobile.navigate("#myprofileview")
            //$fbloginStatus.html('Access Token: ' + data);
        }).fail(function(data) {
            $fbloginStatus.html(data.error);
        });
	});

});
