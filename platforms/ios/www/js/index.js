var googleapi = {
    authorize: function(options) {

        var deferred = $.Deferred();

        //Build the OAuth consent page URL
        var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'code',
            scope: options.scope
        });

        //Open the OAuth consent page in the InAppBrowser
        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

        //The recommendation is to use the redirect_uri "urn:ietf:wg:oauth:2.0:oob"
        //which sets the authorization code in the browser's title. However, we can't
        //access the title of the InAppBrowser.
        //
        //Instead, we pass a bogus redirect_uri of "http://localhost", which means the
        //authorization code will get set in the url. We can access the url in the
        //loadstart and loadstop events. So if we bind the loadstart event, we can
        //find the authorization code and close the InAppBrowser after the user
        //has granted us access to their data.
        $(authWindow).on('loadstart', function(e) {
            var url = e.originalEvent.url;
            var code = /\?code=(.+)$/.exec(url);
            var error = /\?error=(.+)$/.exec(url);

            if (code || error) {
                //Always close the browser when match is found
                authWindow.close();
            }

            if (code) {
                //Exchange the authorization code for an access token
                $.post('https://accounts.google.com/o/oauth2/token', {
                    code: code[1],
                    client_id: options.client_id,
                    client_secret: options.client_secret,
                    redirect_uri: options.redirect_uri,
                    grant_type: 'authorization_code'
                }).done(function(data) {
                    deferred.resolve(data);
                }).fail(function(response) {
                    deferred.reject(response.responseJSON);
                });
            } else if (error) {
                //The user denied access to the app
                deferred.reject({
                    error: error[1]
                });
            }
        });

        return deferred.promise();
    }
};

var fbapi = {
    authorize: function(options) {
        var deferred = $.Deferred();
		//deferred.reject({ error: 'Not Implemented' });

        //Build the OAuth consent page URL
        var authUrl = 'https://www.facebook.com/dialog/oauth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'token',
            scope: options.scope
        });

        //Open the OAuth consent page in the InAppBrowser
        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

		//https://developers.facebook.com/docs/facebook-login/login-flow-for-web-no-jssdk/
		//http://aaronparecki.com/articles/2012/07/29/1/oauth2-simplified
        $(authWindow).on('loadstart', function(e) {
            var url = e.originalEvent.url;
            var token = /#access_token=(.+)$/.exec(url);
            var error = /#error=(.+)$/.exec(url);
			var access_token = token[1]

            if (access_token || error) {
                //Always close the browser when match is found
                authWindow.close();
            }

            if (access_token) {
                //Access token recieved
                deferred.resolve(access_token);
            } else if (error) {
                //The user denied access to the app
                deferred.reject({
                    error: error[1]
                });
            }
        });
        return deferred.promise();
    }
};

$(document).on('deviceready', function() {
    var $GloginButton = $('#google_login');
    var $fbloginButton = $('#fb_login');    
    var $GloginStatus = $('#google p');
	var $fbloginStatus = $('#fb p');
    var $usernameString;
    var $useremailString;
    var $userToken;
    var $response;
    $GloginButton.on('click', function() {
        googleapi.authorize({
            client_id: '10845411425-3v6tsgias7gchqqo3qqhdpbde4ar69q0.apps.googleusercontent.com',
            client_secret: 'PDbrjOEXZK3t4ohf7u5HZ7H4',
            redirect_uri: 'http://localhost',
            scope: 'https://www.googleapis.com/auth/userinfo.email'
            // https://www.googleapis.com/auth/userinfo.email
            // https://www.googleapis.com/auth/analytics.readonly
            //scope: 'https://www.googleapis.com/auth/plus.me'
        }).done(function(data) {
            $userToken = "ya29.1.AADtN_UkLk9GlgsFnIeG1t25JhHZI0TARnkSl66Npst7bMcFbrW60O6Q26GZaj8";
            //$userToken = data.access_token;
            $.get("https://www.googleapis.com/plus/v1/people/me", 
                {fields: "emails", access_token: $userToken} )
                .success(function(resp) {
                    $useremailString = resp['emails'].filter(function(v) {
                    return v.type === 'account'; // Filter out the primary email
                    })[0].value; // get the email from the filtered results, should always be defined.
                    //alert("Welcome " + $useremailString);
                    //document.getElementById('auth_useremail').innerHTML = $useremailString;
                    document.getElementById('auth_useremail').innerHTML = $useremailString;
                });

            //alert($useremailString);
            //document.getElementById('auth_useremail').innerHTML = $useremailString;
            //$GloginStatus.html('Access Token: ' + data.access_token);
            //loadProfile();
            $.mobile.navigate("#myprofileview")
        }).fail(function(data) {
            $GloginStatus.html(data.error);
        });
    });

	$fbloginButton.on('click', function() {
		fbapi.authorize({
            client_id: '1431790103709845',
            client_secret: 'ae0bcfade0a037e472a21e57fd94417b',
            redirect_uri: 'https://kk.bizcard.org',
            scope: 'email,user_photos,user_likes,user_about_me'
        }).done(function(data) {
             $.mobile.navigate("#myprofileview")
            //$fbloginStatus.html('Access Token: ' + data);
        }).fail(function(data) {
            $fbloginStatus.html(data.error);
        });
	});

});
/*
function parseResponse(data) {
//    if (data.is_verified) {

    $.get("https://www.googleapis.com/auth/userinfo/email?alt=json",
        function(resp) {resp.} )
        $useremailString = data.email;
        $userToken = data.access_token;
//    }
     
} */

  // Load the API and make an API call.  Display the results on the screen.
  /*function loadProfile() {
    console.log("In loadProfile");
    gapi.client.load('plus', 'v1', function() {
      var request = gapi.client.plus.people.get( { 'userId': 'me' }) ;
      request.execute(function(resp) {
            $response = resp;
            parseResponse();
      });
    });
  }

  function parseResponse() {
        // Commented out below previously
        var heading = document.createElement('h4');
        var image = document.createElement('img');
        image.src = resp.image.url;
        heading.appendChild(image);
        heading.appendChild(document.createTextNode(resp.displayName));
        document.getElementById('content').appendChild(heading);
        
   
        console.log("In parseResponse");
        $usernameString = $response.displayName;
        alert($response.displayName);
        $useremailString = $response['emails'].filter(function(v) {
            return v.type === 'account'; // Filter out the primary email
        })[0].value; // get the email from the filtered results, should always be defined.

        console.log($usernameString);
        console.log($useremailString);
        $("#auth_username").html($usernameString);
        $("#auth_useremail").html($useremailString);
        //var usernametext = document.getElementById('auth_username').textContent = $usernameString;
        //var useremailtext = document.getElementById('auth_useremail').innerHTML = $useremailString;
  } */
